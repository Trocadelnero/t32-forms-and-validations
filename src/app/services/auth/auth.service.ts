import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }
  /**
   * Register.POST
   * @param user 
   */
  public register(user): Promise<any> {
    return this.http.post('https://survey-poodle.herokuapp.com/v1/api/users/register', {
      user: { ...user }

    }).toPromise()
  }

  /**
  * Login.POST
  * @param user 
  */
  public login(user): Promise<any> {
    return this.http.post('https://survey-poodle.herokuapp.com/v1/api/users/login', {
      user: { ...user }
    }).toPromise()
  }
}




