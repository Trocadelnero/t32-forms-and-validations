import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth/auth.service";
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  registerForm: FormGroup = new FormGroup({

    username: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*'), Validators.minLength(3), Validators.maxLength(11)]),
    password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(11)]),
    confirm: new FormControl('')

  });

  //FormGroup Getters
  get username() {
    return this.registerForm.get('username')
  }
  get password() {
    return this.registerForm.get('password')
  }
  get confirm() {
    return this.registerForm.get('confirm')
  }

  registerError: string;
  registerMessage: string;
  
  /**Register-TOGGLES
   * Confirm password to reveal checkbox.
   * Check box toggles Reg-btn.
   * Register button goes onRegisterClicked()
   * isLoading shows a text + hides Reg-btn during registration
   */
  isLoading: boolean = false;
  showRegister: boolean = false;
  toggleRegister() { this.showRegister = !this.showRegister, this.registerError = '', this.registerMessage = ''; };  //Later: toggle-service?


  constructor(private auth: AuthService, private router: Router) { }


  ngOnInit(): void { }
  /**
   * Try to register.
   * @param registerForm.value
   */
  async onRegisterClicked() {

    try {
      this.toggleRegister()
      this.isLoading = true;
      const result: any = await this.auth.register(this.registerForm.value);
      console.log(result);
      if (result.status < 400) {
        this.registerMessage = 'You are registered, remember your credentials!';
      }
    } catch (e) {
      console.error(e.error);
      this.registerError = e.error.error
    }
    finally {
      this.isLoading = false;
    }

  }

}


