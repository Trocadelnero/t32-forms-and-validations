import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from "src/app/services/auth/auth.service";
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  // user = { username: '', password: '' };  /before RForms

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]),
  });
  //Getters
  get username() {
    return this.loginForm.get('username')
  }
  get password() {
    return this.loginForm.get('password')
  }

  loginMessage: string;
  loginError: string;

  constructor(private auth: AuthService, private router: Router) { }
  ngOnInit(): void { }
  /**
   * Try async login
   * @param loginForm
   */
  async onLoginClicked() {
    this.loginError = '';
    this.loginMessage = '';
    try {
      const result: any = await this.auth.login(this.loginForm.value);
      console.log(result);
      if (result.status < 400) {
        this.loginMessage = `You are logged in!
      redirect will be featured in the near future`;
        // this.router.navigateByUrl('/dashboard'); 
      }
    } catch (e) {
      console.error(e.error);
      return this.loginError = e.error.error
    } finally {

    }
  }
}



