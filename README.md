# Task 32 - Reactive Forms and Validations

## Description

**Implemented Angular Reactive-Forms and Forms-validation for register/login components.**

- **LoginForm.component**
- **RegisterForm.component**

- **auth.Service**
* register(user): Promise<any>
* login(user): Promise<any>
* based on previous project [T31]https://gitlab.com/Trocadelnero/task-31-services-and-http
  Sends an HTTP request using the HttpClient to attempt register/login.

```
FormGroup and validations
registerForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*'), Validators.minLength(3), Validators.maxLength(11)]),
    password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(11)]),
    confirm: new FormControl('')});

loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(6),Validators.maxLength(12)]),});

example validation-message:
    *ngIf="username.invalid && (username.dirty || username.touched)"
    >
      <i *ngIf="username.errors.required">Username is required</i>
      <i *ngIf="username.errors.minlength"
        >Username must be atleast 3 letters</i
      >
      <i *ngIf="username.errors.pattern">Username can only contain letters</i>
      <i *ngIf="username.errors.maxlength"
        >NOTICE: 12 letters is max for username</i
      >


      FIX needed for Validators.maxLength 
      Only way I could get a message for maxlength to show
      was putting validator below actual allowed maxlength
      but results in unable to get valid forms, hence
      removing [disabled]="registerForm.invalid" from register-button
      and keeping "home-made" toggles.
```

**This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.**

## Install dependencies.

Run `npm install @angular/cli`to install dependencies if you cloned this project.

## Development server

Run `ng serve -o` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

### Aknowledgements

- Api provided by our Great Teacher Dewald Els


